from flask import Flask
from flask_restx import Api

# Configuración Inicial Flask
app = Flask(__name__)
api = Api(
        app,
        title='Alan Cornejo',
        version='2.0',
        description='A description largaaaaaa',
    )


from utils.environment import Environment
config = Environment().settingsGeneral()

