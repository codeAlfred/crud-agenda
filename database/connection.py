from utils.environment import Environment

# Creamos una clase para proteger los datos de la conexión a la BD
class DBConnection(Environment):

    def __init__(self):
        # Variables de Entorno de la conexión a la BD
        db = self.settingsDB()
        self.__BD       = db["BD"]
        self.__HOST     = db["HOST"]
        self.__USER     = db["USER"]
        self.__PWD      = db["PWD"]
        self.__PORT     = db["PORT"]
        self.__DATABASE = db["DATABASE"]

        # Variables de Entorno de configuración SQLAlchemy
        sqlAlchemy = self.settingsSQLAlchemy()
        self.__TRACK_MODIFICATIONS = sqlAlchemy["TRACK_MODIFICATIONS"]
        self.__ECHO = sqlAlchemy["ECHO"]



    def run(self):
        # Declarar un dictionary para hacer referencia al un tipo de conexión de BD
        dbConnection = {
            "sqlLite"   : self.__sqlLite(),
            "mysql"     : self.__mysql(),
            "pg"        : self.__postgreSQL(),
        }

        return {
            "URI": dbConnection[self.__BD],
            "TRACK_MODIFICATIONS": self.__TRACK_MODIFICATIONS,
            "ECHO": self.__ECHO
        }

    def __sqlLite(self):
        return "sqlite:///test.db"

    def __mysql(self):
        return f"mysql+mysqlconnector://{self.__USER}:{self.__PWD}@{self.__HOST}:{self.__PORT}/{self.__DATABASE}"

    def __postgreSQL(self):
        return f"postgresql://{self.__USER}:{self.__PWD}@{self.__HOST}:{self.__PORT}/{self.__DATABASE}"


# Instanciar la clase DBConnection
dbConnection = DBConnection().run()
print(dbConnection)