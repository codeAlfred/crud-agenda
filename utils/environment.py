import os
from dotenv import load_dotenv
from distutils.util import strtobool
load_dotenv()

class Environment():

    def settingsGeneral(self):
        return{
            'PORT': int(os.getenv("PORTAPI", 8081)),
            'DEBUG': strtobool(os.getenv('DEBUG', "false"))
        }

    def settingsSQLAlchemy(self):
        return{
            'TRACK_MODIFICATIONS': strtobool(os.getenv("TRACK_MODIFICATIONS", "False")),
            'ECHO': strtobool(os.getenv('ECHO', "True"))
        }


    def settingsDB(self):
        return{
            'BD': os.getenv("BD", 'sqlLite'),
            'HOST': os.getenv("HOST", '127.0.0.1'),
            'DATABASE': os.getenv("DATABASE", 'agenda'),
            'PORT': os.getenv("PORT", 3306),
            'USER': os.getenv("USER", 'root'),
            'PWD': os.getenv("PWD", 'salmos19'),
        }
